# crypto

Gives simple access to encryption, decryption and hashing methods.

See the whole [documentation](https://megatherium.gitlab.io/crypto) or the [coverage report](https://megatherium.gitlab.io/crypto/coverage).

## Getting started

Install the module:

`$ npm install @megatherium/crypto`

Use the module:

```
import assert from 'assert';
import crypto from '@megatherium/crypto';
import {
	getRandomString,
} from '@megatherium/random';

const key = getRandomString(32),
	value = 'test message';
(async() => {
	const encrypted = await crypto.encrypt(key, value),
		decrypted = await crypto.decrypt(key, encrypted);

	assert.strictEqual(decrypted.toString('utf-8'), value);
})();
```

## API (5)

### Exports

- [convertToBuffer](module-convertToBuffer.html)`(value: any): Buffer` Converts any value into a buffer.
- [decrypt](module-decrypt.html)`(key: string, encryptedValue: Buffer): Promise<Buffer>` Decrypts an encrypted buffer.
- [encrypt](module-encrypt.html)`(key: string, value: any): Promise<Buffer>` Encrypts a value with a key.
- [hash](module-hash.html)`(value: any): Promise<string>` Creates a SHA512 checksum from a value.
- [sha1](module-sha1.html)`(value: any): Promise<string>` Creates the SHA1 checksum of a value.

### Scripts

The following scripts can be executed using `npm run`:
- `build` Builds the module.
- `build-docs` Builds the documentation.
- `build-source` Builds the source code.
- `build-tests` Builds test-cases from jsdoc examples.
- `clear` Clears the module from a previous build.
- `clear-coverage` Clears the coverage reports and caches.
- `clear-docs` Clears the previous documentation build.
- `clear-source` Clears the previous source build.
- `clear-tests` Clears the generated jsdoc example test files.
- `fix` Runs all automated fixes.
- `fix-lint` Automatically fixes linting problems.
- `release` Runs semantic release. Meant to be only executed by the CI, not by human users.
- `test` Executes all tests.
- `test-coverage` Generates coverage reports from the test results using [nyc](https://www.npmjs.com/package/nyc).
- `test-deps` Executes a [depcheck](https://www.npmjs.com/package/depcheck).
- `test-e2e` Executes End-to-End-Tests using [cucumber](https://github.com/cucumber/cucumber-js).
- `test-integration` Executes integration tests using [jest](https://jestjs.io/).
- `test-lint` Executes linting tests using [eslint](https://eslint.org/).
- `test-unit` Executes unit tests using [mocha](https://mochajs.org/).
- `update` Checks for dependency updates using [renovate](https://www.npmjs.com/package/renovate).

## Contribution

See [Contribution Guidelines](CONTRIBUTION.md) for more details.

