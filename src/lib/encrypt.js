/*global Buffer*/
import convertToBuffer from './convertToBuffer';
import crypto from 'crypto';
import logger from './logger';

const log = logger.init('encrypt.js');

/**
 * Encrypts a value with a key.
 * @access public
 * @augments crypto
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Encrypt an object</caption>
 * import assert from 'assert';
 * import {
 * 	encrypt,
 * } from '@megatherium/crypto';
 * import {
 * 	getRandomHexString,
 * } from '@megatherium/random';
 *
 * (async() => {
 * 	const key = await getRandomHexString(32),
 * 		originalValue = {
 * 			production: true,
 * 			test: false,
 * 		},
 * 		encryptedValue = await encrypt(key, originalValue);
 * 	assert.notDeepEqual(encryptedValue, originalValue);
 * 	assert.strictEqual(encryptedValue instanceof Buffer, true);
 * 	assert.rejects(encrypt(undefined, 'test'), {
 * 		name: 'ReferenceError',
 * 		message: '"key" is not defined',
 * 	});
 * 	assert.rejects(encrypt('key', 'test'), {
 * 		name: 'TypeError',
 * 		message: '"key" is not a string with a length of 32 or more',
 * 	});
 * })();
 * @license Megatherium Standard License 1.0
 * @module encrypt
 * @param {string} key The key to encrypt the value with; has to be at least of a length of 32 characters. Limited to a length of 32 characters.
 * @param {any} value The value to encrypt.
 * @returns {Promise<Buffer>} Returns the encrypted value as a buffer.
 * @throws {ReferenceError} Throws a ReferenceError with `'"key" is not defined'` if the key is not given or to slow.
 * @throws {TypeError} Throws a TypeError with `'"key" is not a string with a length of 32 or more'` if the given key has an invalid format.
 */
const encrypt = async(key, value) => {
	if (!key) {
		throw new ReferenceError('"key" is not defined');
	}
	if (typeof key !== 'string' || key.length < 32) {
		throw new TypeError('"key" is not a string with a length of 32 or more');
	}

	const algorithm = 'aes256',
		iv = crypto.randomBytes(16),
		cipher = crypto.createCipheriv(algorithm, key.substring(0, 32), iv),
		meta = {
			algorithm,
			iv: iv.toString('base64'),
		},
		metaJson = JSON.stringify(meta),
		encrypted = Buffer.concat([
			Buffer.from(`${ metaJson.length };${ metaJson }`),
			cipher.update(convertToBuffer(value)),
			cipher.final(),
		]);
	return encrypted;
};

export default encrypt;
