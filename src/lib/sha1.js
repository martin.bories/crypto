import convertToBuffer from './convertToBuffer';
import crypto from 'crypto';
import logger from './logger';

const log = logger.init('sha1.js');

/**
 * Creates the SHA1 checksum of a value.
 * @access public
 * @augments crypto
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Create the SHA1 checksum of a String</caption>
 * import assert from 'assert';
 * import {
 * 	sha1,
 * } from '@megatherium/crypto';
 *
 * (async() => {
 * 	const originalValue = 'test',
 * 		encrypted = await sha1(originalValue);
 *
 * 	assert.equal(typeof encrypted, 'string');
 * 	assert.notEqual(encrypted, originalValue);
 * })();
 * @license Megatherium Standard License 1.0
 * @module sha1
 * @param {any} value The value to encrypt.
 * @returns {Promise<string>} Returns the encrypted value as hex-string.
 */
const sha1 = (value) => {
	const hash = crypto.createHash('sha1');
	hash.update(convertToBuffer(value));

	return hash.digest('hex');
};

export default sha1;
