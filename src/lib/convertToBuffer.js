/*global Buffer*/
import logger from './logger';

const log = logger.init('convertToBuffer.js');

/**
 * Converts any value into a buffer.
 * @access public
 * @augments Buffer.from
 * @augments JSON.stringify
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Convert a boolean into a buffer</caption>
 * //global Buffer
 * import assert from 'assert';
 * import {
 * 	convertToBuffer,
 * } from '@megatherium/crypto';
 *
 * const asBuffer = convertToBuffer(true);
 * assert.strictEqual(asBuffer instanceof Buffer, true);
 * assert.strictEqual(convertToBuffer(null) instanceof Buffer, true);
 * @license Megatherium Standard License 1.0
 * @module convertToBuffer
 * @param {any} value The value to convert into a Buffer.
 * @returns {Buffer} The value as a Buffer.
 */
const convertToBuffer = (value) => {
	if (value instanceof Buffer) {
		return value;
	} else if (typeof value === 'string') {
		return Buffer.from(value);
	} else if (typeof value === 'number') {
		return Buffer.from(value.toString());
	} else if (typeof value === 'function') {
		return Buffer.from(value.toString());
	} else if (value === null) {
		return Buffer.from('null');
	} else if (value === undefined) {
		return Buffer.from('undefined');
	} else if (typeof value !== 'object' && value !== undefined && value.toString && typeof value.toString === 'function') {
		return Buffer.from(value.toString());
	}
	return Buffer.from(JSON.stringify(value));
};

export default convertToBuffer;
