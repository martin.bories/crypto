/*global Buffer*/
import crypto from 'crypto';
import logger from './logger';

const log = logger.init('decrypt.js');

/**
 * Decrypts an encrypted buffer.
 * @access public
 * @augments crypto
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Encrypt and decrypt a text</caption>
 * import assert from 'assert';
 * import {
 * 	decrypt,
 * 	encrypt,
 * } from '@megatherium/crypto';
 * import {
 * 	getRandomHexString,
 * } from '@megatherium/random';
 *
 * (async() => {
 * 	const key = await getRandomHexString(32),
 * 		value = 'Hello, World!',
 * 		encryptedValue = await encrypt(key, value),
 * 		decryptedValue = await decrypt(key, encryptedValue);
 *
 * 	assert.equal(decryptedValue.toString('utf-8'), value);
 * })();
 * @license Megatherium Standard License 1.0
 * @module decrypt
 * @param {string} key The key used for decryption.
 * @param {Buffer} encryptedValue The encrypted value being decrypted.
 * @returns {Promise<Buffer>} Returns the decrypted value.
 */
const decrypt = async(key, encryptedValue) => {
	// extract meta
	const metaLengthEndIndex = encryptedValue.indexOf(';'),
		metaLength = parseInt(encryptedValue.slice(0, metaLengthEndIndex).toString('utf-8')),
		metaEndIndex = metaLengthEndIndex + 1 + metaLength,
		metaJson = encryptedValue.slice(metaLengthEndIndex + 1, metaEndIndex),
		meta = JSON.parse(metaJson),
		algorithm = meta.algorithm,
		iv = Buffer.from(meta.iv, 'base64'),
		encrypted = encryptedValue.slice(metaEndIndex),
		decipher = crypto.createDecipheriv(algorithm, key.substring(0, 32), iv),
		decrypted = Buffer.concat([
			decipher.update(encrypted),
			decipher.final(),
		]);
	return decrypted;
};

export default decrypt;
