import convertToBuffer from './lib/convertToBuffer';
import decrypt from './lib/decrypt';
import encrypt from './lib/encrypt';
import hash from './lib/hash';
import sha1 from './lib/sha1';

const crypto = {
	/**
	 * Converts any value into a buffer.
	 * @access public
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Convert a boolean into a buffer</caption>
	 * import {
	 * 	convertToBuffer,
	 * } from '@megatherium/crypto';
	 *
	 * const asBuffer = convertToBuffer(true);
	 * @license Megatherium Standard License 1.0
	 * @param {any} value The value to convert into a Buffer.
	 * @returns {Buffer} The value as a Buffer.
	 * @throws {Error} Throws an error if the value cannot be converted into a Buffer.
	 */
	convertToBuffer,

	/**
	 * Decrypts an encrypted buffer.
	 * @access public
	 * @augments crypto
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Encrypt and decrypt a text</caption>
	 * import {
	 * 	decrypt,
	 * 	encrypt,
	 * } from '@megatherium/crypto';
	 * import {
	 * 	getRandomHexString,
	 * } from '@megatherium/random';
	 *
	 * (async() => {
	 * 	const key = await getRandomHexString(32),
	 * 		value = 'Hello, World!',
	 * 		encryptedValue = await encrypt(key, value),
	 * 		decryptedValue = await decrypt(key, encryptedValue);
	 *
	 * 	assert.equal(decryptedValue.toString('utf-8'), value);
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {string} key The key used for decryption.
	 * @param {Buffer} encryptedValue The encrypted value being decrypted.
	 * @returns {Promise<Buffer>} Returns the decrypted value.
	 */
	decrypt,

	/**
	 * Encrypts a value with a key.
	 * @access public
	 * @augments crypto
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Encrypt an object</caption>
	 * import {
	 * 	encrypt,
	 * } from '@megatherium/crypto';
	 * import {
	 * 	getRandomHexString,
	 * } from '@megatherium/random';
	 *
	 * (async() => {
	 * 	const key = await getRandomHexString(32),
	 * 		encryptedValue = await encrypt(key, {
	 * 			production: true,
	 * 			test: false,
	 * 		});
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {string} key The key to encrypt the value with; has to be at least of a length of 32 characters. Limited to a length of 32 characters.
	 * @param {any} value The value to encrypt.
	 * @returns {Promise<Buffer>} Returns the encrypted value as a buffer.
	 * @throws {Error} If the key is not given or to slow.
	 */
	encrypt,

	/**
	 * Creates a SHA512 checksum from a value.
	 * @access public
	 * @augments crypto
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Create a checksum</caption>
	 * import {
	 * 	hash,
	 * } from '@megatherium/crypto';
	 *
	 * (async() => {
	 * 	const encryptedValue = await hash(true);
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {any} value The value to create the checksum from.
	 * @returns {Promise<string>} Returns the encrypted value as a hex-string.
	 */
	hash,

	/**
	 * Creates the SHA1 checksum of a value.
	 * @access public
	 * @augments crypto
	 * @author Martin Bories <martin.bories@megatherium.solutions>
	 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
	 * @example <caption>Create the SHA1 checksum of a String</caption>
	 * import {
	 * 	sha1,
	 * } from '@megatherium/crypto';
	 *
	 * (async() => {
	 * 	const encrypted = await sha1('test');
	 * })();
	 * @license Megatherium Standard License 1.0
	 * @param {any} value The value to encrypt.
	 * @returns {Promise<string>} Returns the encrypted value as hex-string.
	 */
	sha1,
};

export {
	convertToBuffer,
	decrypt,
	encrypt,
	hash,
	sha1,
};

export default crypto;
