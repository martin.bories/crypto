import log from '@megatherium/log';

const logger = log.init({
	app: '@megatherium/crypto',
});

export default logger;
