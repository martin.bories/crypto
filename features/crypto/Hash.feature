Feature: Hash

	Scenario: Creating a hash out of integer
		Given a random integer
		When I hash it
		Then the hash should not contain it
	
	Scenario: Creating two hashes out of integers
		Given 2 random integers
		When I hash them
		Then the hashes should not contain them
	
	Scenario: Creating a hash out of number
		Given a random number
		When I hash it
		Then the hash should not contain it
	
	Scenario: Creating two hashes out of numbers
		Given 2 random numbers
		When I hash them
		Then the hashes should not contain them

	Scenario: Creating a hash out of string
		Given a random string
		When I hash it
		Then the hash should not contain it
	
	Scenario: Creating two hashes out of strings
		Given 2 random strings
		When I hash them
		Then the hashes should not contain them