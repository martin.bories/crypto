const {
	setWorldConstructor,
} = require('@megatherium/test').cucumber;

class World {

	constructor() {
		this.key = '12345678901234567890123456789012';
		this.plain = [];
		this.hashed = [];
		this.encrypted = [];
		this.decrypted = [];
	}

};

setWorldConstructor(World);