const {
	assert,
} = require('@megatherium/test');
const crypto = require('../../dist/');
const {
	Given,
	Then,
	When
} = require('@megatherium/test').cucumber;
const random = require('@megatherium/random');

Given(/^the key "(.*)"$/, function(key) {
	this.key = key;
});

Given(/^a random ([a-zA-Z]+)$/, function(type) {
	const methodName = `getRandom${ type.charAt(0).toUpperCase() }${ type.substring(1) }`;
	this.plain.push(random[methodName]());
});

Given(/^([1-9][0-9]*) random ([a-zA-Z]+)s$/, function(amount, type) {
	amount = parseInt(amount);
	const methodName = `getRandom${ type.charAt(0).toUpperCase() }${ type.substring(1) }`;
	for (let i = 0; i < amount; ++i)
		this.plain.push(random[methodName]());
});

When(/^I ([a-zA-Z][a-zA-Z0-9]*) (?:it|them)$/, async function(methodName) {
	for (let i = 0; i < this.plain.length; ++i) {
		const plain = this.plain[i];
		if (methodName === 'hash'
			|| methodName === 'sha1')
			this.hashed[i] = await crypto[methodName](plain);
		else
			this[`${ methodName }ed`][i] = await crypto[methodName](this.key, methodName === 'encrypt' ? plain : this.encrypted[i]);
	}
});

Then(/^the (hash|encrypt|decrypt)[eds]* should( not)? contain (?:it|them)$/, function(propertyName, not) {
	const list = this[`${ propertyName }ed`];
	this.plain.forEach(plain => {
		if (not)
			assert.containsNot(list, plain);
		else
			assert.contains(list, plain);
	});
});